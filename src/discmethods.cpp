

#define WIDTH 9
#define PRECISION 6

#include <iostream>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <exception>
#include <stdexcept>
#include "parameters.h"
using namespace std;




dim_p_modeldata explicit_p_model_disc(dim_p_modeldata inptdata, ofstream &otptfile, bool fileverbose, bool screenverbose){
    
    //Data structure initializations
    dim_p_modeldata olddata,newdata;
    olddata=inptdata;
    newdata=inptdata;

    
    double newpress,alpha,beta;
    
    beta = olddata.delttg() / (olddata.deltrg()*olddata.deltrg());
    cout << beta << endl;
    for (int t = 1; t<=inptdata.num_timestepsg(); t++) {
        for (int r=1; r<(inptdata.num_rstepsg()-1); r++) {
            
            alpha = olddata.delttg() / (olddata.deltrg()* .5 * (olddata.rdg(r+1)+olddata.rdg(r)));
            cout << alpha << endl;
            newpress = olddata.pressg(r) + alpha * (olddata.pressg(r+1)-olddata.pressg(r)) + beta * (olddata.pressg(r+1)-2*olddata.pressg(r)+olddata.pressg(r-1));
            
            newdata.presss(r,newpress);
            
        }

        
        //Output at end of timestep. File output only occurs if fileverbose argument is true; screen output only occurs if screenverbose argument is true
        newdata.tsteps(t);
        if (fileverbose && (t % newdata.output_freqg()==0)) {newdata.statusprint(otptfile,0);}
        if (screenverbose && (t % newdata.output_freqg()==0)) {newdata.statusprint(0);}
        
        olddata = newdata;
        
    }
    //Return final data structure
    return newdata;
} //Explicit case functions




