#define WIDTH 9
#define PRECISION 6

#include <iostream>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <exception>
#include <stdexcept>
using namespace std;


//Main data structure with pressure and radius vectors
class dim_p_modeldata {
    
private:
    double *press,*rd;
    double deltr,deltt,re,ro;
    int tstep, num_rsteps, num_timesteps, output_freq;
    
public:
    //Set size method to initialize pressure and radius vectors
    void vectinit(int r) {
        press = new double[r];
        rd = new double[r];
        
        rd[0] = 1;
        rd[num_rsteps-1] = re/ro;
        deltr = (rd[num_rsteps-1]-rd[0])/(r-1);
        
        num_rsteps= r;
        
        for(int i = 0; i < num_rsteps; i++){
            rd[i] =  1 + i*deltr;
            
            if (i==0) {
                press[i] = 0;
            } else {
                press[i] = 1;
            }
        }
    }
    
    
    
    //Screen print method - 0 indicates timestep temp output, 1 indicates fully verbose output
    void statusprint(int type) {
        //Verbose output
        if (type) {
            int i,j;
            cout << "Size: " << num_rsteps << endl;
            cout << "Desired Number of Timesteps: " << num_timesteps << endl;
            cout << "Output frequency: " << output_freq << endl;
            
            cout.precision(PRECISION); cout << fixed << right;
            cout << "ro: " << ro << endl;
            cout << "re: " << re << endl;
            
            //rD vector output
            cout << "Dimensionless Radius values: " << endl;
            for (i=0; i<num_rsteps; i++) {
                cout << " ";
                cout.width(WIDTH); cout << left;
                cout << rd[i] << "  ";
            }
            cout << endl;
            
            //Temperature matrix output
            cout << "Timestep #" << tstep << endl;
            cout << "Pressure: " << endl;
            for (i=0; i<num_rsteps; i++) {
                cout.width(WIDTH); cout.precision(PRECISION); cout << right;
                cout << press[i] << "  ";
            }
            cout << endl;
        }
        
        //Timestep temp only output
        else {
            int i,j;
            for (i=1; i<=((num_rsteps*(WIDTH+2)-14)/2); i++) { cout << "="; }
            cout << " TIMESTEP #" << tstep << " ";
            for (i=1; i<=((num_rsteps*(WIDTH+2)-14)/2); i++) { cout << "="; }
            cout << endl;
            
            //Temperature matrix output
            for (i=0; i<num_rsteps; i++) {
                cout.width(WIDTH); cout.precision(PRECISION); cout << right;
                cout << press[i] << "  ";
            }
            cout << endl;
        }
    }
    
    
    //File print method - 0 indicates timestep temp output, 1 indicates fully verbose output
    void statusprint(ofstream &file, int type){
        if (file.good()) {
            //Verbose output
            //Verbose output
            if (type) {
                int i,j;
                file << "Size: " << num_rsteps << endl;
                file << "Desired Number of Timesteps: " << num_timesteps << endl;
                file << "Output frequency: " << output_freq << endl;
                
                file.precision(PRECISION); file << fixed << right;
                file << "ro: " << ro << endl;
                file << "re: " << re << endl;
                
                //rD vector output
                file << "Dimensionless Radius values: " << endl;
                for (i=0; i<num_rsteps; i++) {
                    file << " ";
                    file.width(WIDTH); file << left;
                    file << rd[i] << "  ";
                }
                file << endl;
                
                //Temperature matrix output
                file << "Timestep #" << tstep << endl;
                file << "Pressure: " << endl;
                for (i=0; i<num_rsteps; i++) {
                    file.width(WIDTH); file.precision(PRECISION); file << right;
                    file << press[i] << "  ";
                }
                file << endl;
            }
            
            //Timestep temp only output
            else {
                int i,j;
                for (i=1; i<=((num_rsteps*(WIDTH+2)-14)/2); i++) { file << "="; }
                file << " TIMESTEP #" << tstep << " ";
                for (i=1; i<=((num_rsteps*(WIDTH+2)-14)/2); i++) { file << "="; }
                file << endl;
                
                //Temperature matrix output
                for (i=0; i<num_rsteps; i++) {
                    file.width(WIDTH); file.precision(PRECISION); file << right;
                    file << press[i] << "  ";
                }
                file << endl;
            }
        }
    }
    
    
    //"=" Operator overloader definition
    dim_p_modeldata& operator=(dim_p_modeldata other){
        num_timesteps = other.num_timesteps;
        num_rsteps = other.num_rsteps;
        tstep = other.tstep;
        deltr = other.deltr;
        deltt = other.deltt;
        re = other.re;
        ro = other.ro;
        output_freq = other.output_freq;
        
        
        press = new double[num_rsteps];
        rd = new double[num_rsteps];
        
        for(int r = 0; r < num_rsteps; r++){
            press[r] =  other.pressg(r);
            rd[r] =  other.rdg(r);
        }
        return *this;
    }
    
    //Setter and Getter Functions
    void presss(int r, double p)            { press[r] = p; }
    double pressg(int r)                    { return press[r]; }
    void rds(int r, double dimradius)       { rd[r] = dimradius; }
    double rdg(int r)                          { return rd[r]; }
    void num_rstepss(int num)               { num_rsteps = num; }
    int num_rstepsg()                       {return num_rsteps; }
    void num_timestepss(int num)            { num_timesteps = num; }
    int num_timestepsg()                    { return num_timesteps; }
    void tsteps(int num)                    { tstep = num; }
    int tstepg()                            { return tstep; }
    void output_freqs(int freq)             { output_freq = freq; }
    int output_freqg()                      { return output_freq; }
    double delttg()                         { return deltt; }
    void deltts(double dt)                         { deltt = dt; }
    double deltrg()                         { return deltr; }
    void res(double r)                         { re = r; }
    void ros(double r)                         { ro = r; }
};



dim_p_modeldata explicit_p_model_disc(dim_p_modeldata inptdata, std::ofstream &otptfile, bool fileverbose, bool screenverbose);
dim_p_modeldata modelinit(int nrsteps,double re,double ro,int numt,double dt,int otpf);
void fileprintmaster(char outfile[], std::ofstream &otptfile, dim_p_modeldata data, bool cond);
