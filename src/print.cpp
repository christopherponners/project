

#define WIDTH 9
#define PRECISION 6

#include <iostream>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <exception>
#include <stdexcept>
#include "parameters.h"
using namespace std;


//Function to read initialize model; returns initialized "dim_p_modeldata" structure
dim_p_modeldata modelinit(int nrsteps,double re,double ro,int numt,double dt,int otpf){
    dim_p_modeldata scratchdata;      //Scratch modeldata object
    
    scratchdata.res(re);
    scratchdata.ros(ro);
    
    scratchdata.vectinit(nrsteps);
    
    scratchdata.num_timestepss(numt);
    scratchdata.deltts(dt);
    scratchdata.output_freqs(otpf);
    
    
    
    return scratchdata;
}






//Master print to file - Final argument 1 used for file initialization; 0 used for final finalization
void fileprintmaster(char outfile[], ofstream &otptfile, dim_p_modeldata data, bool cond) {
    //File initialization
    if (cond) {
        otptfile.open(outfile);
        if (otptfile.good()) {
            
            //File header
            otptfile << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~RADIAL AQUIFER DIFUSIVITY MODEL OUTPUT~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n";
            otptfile << "The following file includes the solution to the radial difusivity equation in 1 dimensional r space over time.  Discretization was performed using explicit finite diference approximations.\n";
            otptfile << endl << "Created by: Christopher Ponners\n" << "Code Last Modified: 12/4/2015\n";
            otptfile << "Written as class project for Scientific and Technical Computing, Fall 2015\n";
            otptfile << "Texas Advanced Computing Center, The University of Texas at Austin\n";
            otptfile << "=====================================================================================================\n\n";
            
            otptfile << "Writing to output file '" << outfile << "'\n";
            
            //Obtaining and outputting timestamp
            time_t now = time(0);
            tm* loctm = localtime(&now);
            otptfile << "Model began running: " << asctime(loctm) << endl << endl;
            
            //Initial conditions output
            otptfile <<"Initial Model Conditions:" << endl << endl;
            data.statusprint(otptfile,1);
            otptfile << endl << "Beginning Calculation..." << endl;
        }
    }
    
    //Post analysis file outputs
    else {
        if (otptfile.good()) {
        otptfile << "==========================================================================================\n\n";
            
        //Final conditions output
        otptfile << "Final Model Conditions:" << endl << endl;
        data.statusprint(otptfile,1);
            
        otptfile.close();
        }
    }
}




