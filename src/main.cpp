

#define WIDTH 9
#define PRECISION 6

#include <iostream>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <exception>
#include <stdexcept>
#include "parameters.h"
using namespace std;

#define SIZER 10
#define RE 5
#define RO 1
#define NUMT 10
#define DT 1
#define OTPF 1


//Main program function
int main(int argc, char *argv[]){
    
    //Test valid # of arguments
    if (argc != 3){
        cout << "\nInsufficient number of arguments.\nUsage:" << argv[0] << " <output frequency> <output file>\n";
        return -1;
    }
    
    try {
        
        //Initialize data structures
        dim_p_modeldata finaldata,maindata;
        maindata = modelinit(SIZER, RE, RO, NUMT, DT, atoi(argv[1]));
        
        ofstream outputfile;
        
        //Initial screen/file output
        cout << "Initial Model Conditions:"  << endl << endl;
        maindata.statusprint(1);
        fileprintmaster(argv[2],outputfile,maindata,1);
        
        //Calculations
        finaldata = explicit_p_model_disc(maindata, outputfile,1,1);
        
        //Post screen/file output
        fileprintmaster(argv[2],outputfile,finaldata,0);
        cout << "Final Model Conditions:" << endl << endl;
        finaldata.statusprint(1);
    }
    
    //Main catch for logical errors including incomplete definition of starting conditions
    catch (logic_error& e) {
        cout << e.what() << " Terminating." << endl;
        return -2;
    }
    
    //Main catch for conflicting inputs
    catch (streampos& e) {
        cout << "Error. Input file overwriting previously defined point at pointer " << e << ". Terminating." << endl;
        return -3;
        //Note: This catch will catch a throw (at pointer -1) and terminate if there are any trailing lines (carriage returns) at the end of the input document. Last line of input file must have data.
    }
    
    //Main catch for output errors
    catch (ios_base::failure& e) {
        cout << "Error. File output not completed. Terminating. \n" << e.what() << endl;
        return -4;
    }
    
    //Catch for other errors
    catch (...) {
        cout << "Unspecified error has occurred. Terminating. \n";
        return -5;
    }
    return 0;
}
