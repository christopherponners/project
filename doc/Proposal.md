# Scientific and Technical Computing - Project Proposal

Christopher Ponners
TR 2-3:30 PM
October 30, 2015


### Proposal Description:
The proposed project is an exploration into the numerical solution of petroleum reservoir aquifer models. Currently, four models are to be analyzed:

1. Pot Aquifer Model
2. Fetkovitch Aquifer Model
3. Carter-Tracy Aquifer Model
4. Van Everdingen-Hurst Aquifer Model

#### Domain Science Background
In geosystems analysis, there are a variety of drive mechanisms that serve to create reservoir characteristics over geological time which greatly effect the pressure response that reservoirs experience when fluids are withdrawn or injected.  For reservoir engineers, the ability to accurately analyze and quantify these drive mechanisms are vital to the accuracy of geosystems models and simulations. 

Water influx models are mathematical models that attempt to describe aquifer performance, and when integrated with reservoir simulators, can effectively simulate the performance of water drive reservoirs by representing the complex pressure gradients that cause flow within a reservoir and at reservoir/aquifer boundaries.  As with many mathematical models of natural phenomena, increased model accuracy necessitates increased model complexity.  With respect to obtaining solutions, this can sometimes make analytical solutions impossible and numerical solutions that require greater computational power.

The use of these models was applied through the analytical solution to applicable differential equations to determine water influx with respect to time given the case of a constant pressure differential at the reservoir/aquifer contact.  Historically, these solutions were presented in charts or tables on a dimensionless basis.  Many software packages continue to utilize digitized versions of these charts in water influx calculations.  Discretization of the time and pressure domains is then combined with the concept of superposition in order to derive the total water influx rate over time given a reservoir pressure history.

#### Computational Goals
The main goal will be to create computational solutions to the various models utilizing numerical methods to derive the solution to the differential equations. The various models will be compared for computational efficiency (i.e. increased computational time per increased degree of accuracy), storage efficiency, and other means of optimization. Additional goals include:

* Creation of post processing graphical output
* Start/Stop/Restart methods
* Development of history matching algorithms


### Member Information:
* Name: Christopher Ponners
* UT eID: cgp538
* Stampede userid: cponners
* Email: <chrisponners@utexas.edu>
* BitBucket Repository: <https://bitbucket.org/christopherponners/stc>
* UT Classification: Undergrad, 3rd Year
* Major: Petroleum Engineering