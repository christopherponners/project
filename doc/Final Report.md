# Scientific and Technical Computing - Project Final Report
Christopher Ponners
TR 2-3:30 PM


### Proposal Description:
The proposed project is an exploration into the numerical solution of petroleum reservoir aquifer models. Currently, four models are to be analyzed:

1. Pot Aquifer Model
2. Fetkovitch Aquifer Model
3. Carter-Tracy Aquifer Model
4. Van Everdingen-Hurst Aquifer Model

#### Domain Science Background
In geosystems analysis, there are a variety of drive mechanisms that serve to create reservoir characteristics over geological time which greatly effect the pressure response that reservoirs experience when fluids are withdrawn or injected.  For reservoir engineers, the ability to accurately analyze and quantify these drive mechanisms are vital to the accuracy of geosystems models and simulations. 

Water influx models are mathematical models that attempt to describe aquifer performance, and when integrated with reservoir simulators, can effectively simulate the performance of water drive reservoirs by representing the complex pressure gradients that cause flow within a reservoir and at reservoir/aquifer boundaries.  As with many mathematical models of natural phenomena, increased model accuracy necessitates increased model complexity.  With respect to obtaining solutions, this can sometimes make analytical solutions impossible and numerical solutions that require greater computational power.

The use of these models was applied through the analytical solution to applicable differential equations to determine water influx with respect to time given the case of a constant pressure differential at the reservoir/aquifer contact.  Historically, these solutions were presented in charts or tables on a dimensionless basis.  Many software packages continue to utilize digitized versions of these charts in water influx calculations.  Discretization of the time and pressure domains is then combined with the concept of superposition in order to derive the total water influx rate over time given a reservoir pressure history.

The difusivity equation --derived from mass ballance considerations and compatable with the Darcy flow equations -- is the fundamental governing equation for radial flow which is applicable to both reservoirs and aquifer engineering. This second order parabolic partial differential equation in radius and time describes the pressure wave that propogates radially outward given an initial pressure drop (due to producing or some means of fluid voidage) at an internally boundary.  Numerically, this equation in the context of aquifer modeling has been solved analytically for both constant terminal pressure and constant terminal rate cases.  Constant terminal pressure solutions are able to be used to dertermine cumulative water influx as a function of time and are highly utilized in aquifer modeling.  Constant terminal rate solutions are able to be used to dertermine the pressure response due to the injection or production of fluid as a function of time, and is often used in modeling shut-in tests or in pressure transient analysis.

Analytically, the diffusivity equation can be solved with two main analytical methods. One method utilizes Laplace transforms well the other utilises a eigenvalue expansion Boltzman transformation. Both methods result in a similar analytical solution that involves Bessell functions of the first and second kind and zero and the first order. Computationally, libraries to compute the values of these functions and the associated eigenvalues and the linear equations in which they are involved, should be readily available. If, however, these such packages are not present, there are two other methods which exist to solve the diffusivity equation. One method involves the use of pre-calculated solutions to the equation previously loaded in tables. In performing the water influx calculations, the dimensionless diffusivity solutions can be read from the tables and interpolated as necessary. Historically this has been the method of choice.  The alternate method, and the one explored in this project, is simply the numerical solution of the diffusivity equation. This method has the advantage of not necessitating a constant terminal rate or constant terminal pressure solution.  Theoretically, pressure steps from a reservoir simulator or other analytical model could be that in simultaneously to the diffusivity equations solver for the aquifer.  This would allow the principal of super position not to be needed and could, if implemented correctly, result in the more accurate solutions.

![Dimensionless difusivity equation](http://patentimages.storage.googleapis.com/WO2007134598A1/imgf000035_0001.png)

#### Computational Methods
The numerical methods used in this project to solve the diffusivity equation are varied. A finite difference method with forward in space approximations to the first and second derivatives was found to be unstable.  While the explicit nature of this method made it computationally simple, the independent timestamps proved unable to provide an adequate solution. The next method used was an implicit method which related the spatial derivative at the new timestamp to the temporal derivative at the old timestamp.  This algorithm results in a system of equations who's number matches the number of nodes (i.e. radii) used in the model. The resulting system is formatted as a tridiagonal matrix and solved using the LAPACK++ libraries.

#### Further Studies to Be Undertaken
With this analysis performed and a solution to the dimensionless pressure as a function of dimensionless time and dimensionless radius present, the king of water influx can be performed through numerical integration. With the function grids already in place it would be simple to use numerical operator formalism to perform this numerical integration.

Other further analysis that should be performed is the Study of the numerous partial differential methods available. Spectral methods, various forks of finite different methods, finite element methods, and others can all be used in obtain solutions to the dimensionless diffusivity equation. With adequate profiling and debugging implemented these numerical solutions can be compared to interpolation from tables and superposition methods as well as other methods in determining cumulative influx, including approximate polinomials that can be fitted to match the constant terminal pressure solutions.

### Member Information:
* Name: Christopher Ponners
* UT eID: cgp538
* Stampede userid: cponners
* Email: <chrisponners@utexas.edu>
* BitBucket Repository: <https://bitbucket.org/christopherponners/stc>
* UT Classification: Undergrad, 3rd Year
* Major: Petroleum Engineering